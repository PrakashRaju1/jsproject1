const problem1 =(arg1,arg2)=>{
    if(arg1===undefined || arg2===undefined || arg1.length===0){
        return [];
    }
    for (let i=0; i<arg1.length; i++){
        if (arg1[i].id === arg2){
            return arg1[i];
        }
        
    }
}

module.exports =problem1;